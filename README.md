# discord-oidc

Fill in `.env` file according to the `envf!` fields in `src/config.rs`, converted to uppercase. Then run build and run like so:

```bash
cargo run
```

## Definitions

- RP: Relying Party, application requiring auth and claims from an OP
- OP: OpenID Provider, this application

## Generate a keypair for JWT

```bash
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
```
