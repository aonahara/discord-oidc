macro_rules! envf {
    ($name:ident) => {
        pub fn $name() -> String {
            let env_name = &stringify!($name).to_ascii_uppercase();
            std::env::var(env_name)
                .expect(&format!("Environment variable {} is required", env_name).to_owned())
        }
    };
}

macro_rules! urlf {
    ($name:ident, $path:expr) => {
        pub fn $name() -> url::Url {
            let mut url = url::Url::parse(&base_url()).expect("Invalid base URL");
            for part in $path.iter() {
                url.path_segments_mut().expect("URL Segment").push(part);
            }
            url
        }
    };
}

envf!(base_url);
envf!(discord_client_id);
envf!(discord_client_secret);
envf!(discord_guild_id);
envf!(discord_bot_token);
envf!(rp_client_id);
envf!(rp_client_secret);
envf!(rp_redirect_url);
envf!(jwt_rsa_sk_pem);
envf!(jwt_rsa_pk_pem);

urlf!(op_issuer_url, vec![""]);
urlf!(op_auth_url, vec!["authorize"]);
urlf!(op_jwk_url, vec![".well-known", "jwks.json"]);
urlf!(op_token_url, vec!["token"]);
urlf!(op_userinfo_url, vec!["userinfo"]);
urlf!(op_redirect_url, vec!["callback"]);
