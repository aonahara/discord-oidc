use crate::config;
use crate::oauth2;
use reqwest::Client;
use url::Url;

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct User {
    pub id: String,
    pub username: String,
    pub discriminator: String,
    pub avatar: String,
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct GuildMember {
    pub user: User,
    pub roles: Vec<String>,
}

fn get_api_url(parts: Vec<&str>) -> Url {
    let mut url = Url::parse("https://discord.com/api").expect("Invalid Discord API URL");
    for part in parts {
        url.path_segments_mut().expect("URL Segment").push(part);
    }
    url
}

pub fn get_oauth2_url(state: Option<&str>) -> Url {
    let mut url = get_api_url(vec!["oauth2", "authorize"]);
    let mut pairs = url.query_pairs_mut();

    pairs
        .append_pair("client_id", &config::discord_client_id())
        .append_pair("response_type", "code")
        .append_pair("scope", "identify guilds")
        .append_pair("redirect_uri", &config::op_redirect_url().to_string())
        .append_pair("prompt", "consent");

    if let Some(state) = state {
        pairs.append_pair("state", state);
    }

    pairs.finish().to_owned()
}

pub async fn exchange_code(client: &Client, code: &str) -> anyhow::Result<oauth2::TokenResponse> {
    let form = oauth2::TokenRequest {
        client_id: config::discord_client_id(),
        client_secret: config::discord_client_secret(),
        grant_type: oauth2::GrantType::AuthorizationCode,
        code: code.to_string(),
        redirect_uri: config::op_redirect_url().to_string(),
    };

    let request = client
        .post(get_api_url(vec!["oauth2", "token"]))
        .form(&form);

    let response = request.send().await?;
    let text = response.text().await?;

    let json: oauth2::TokenResponse = rocket::serde::json::serde_json::from_str(&text)?;

    Ok(json)
}

pub async fn get_user_info(client: &Client, access_token: &str) -> anyhow::Result<User> {
    println!("get_user_info token: {:#?}", access_token);

    let request = client
        .get(get_api_url(vec!["users", "@me"]))
        .bearer_auth(access_token);

    let response = request.send().await?;
    let text = response.text().await?;
    println!("{:#?}", text);

    let json: User = rocket::serde::json::serde_json::from_str(&text)?;
    println!("{:#?}", json);

    Ok(json)
}

pub async fn get_guild_member(client: &Client, user_id: &str) -> anyhow::Result<GuildMember> {
    println!("get_guild_member uid: {:#?}", user_id);

    let url = get_api_url(vec![
        "guilds",
        &config::discord_guild_id(),
        "members",
        user_id,
    ]);

    let request = client.get(url).header(
        "authorization",
        format!("Bot {}", &config::discord_bot_token()),
    );

    println!("get_guild_member req: {:#?}", request);

    let response = request.send().await?;
    let text = response.text().await?;
    println!("{:#?}", text);

    let json: GuildMember = rocket::serde::json::serde_json::from_str(&text)?;
    println!("{:#?}", json);

    Ok(json)
}
