pub mod discord;

pub type ClientId = String;
pub type ClientSecret = String;
pub type AuthorizationCode = String;
pub type RedirectUrl = String;
pub type AccessToken = String;
pub type RefreshToken = String;
pub type IDToken = String;
pub type Nonce = String;

#[derive(serde::Serialize, serde::Deserialize, rocket::FromFormField, Debug)]
pub enum GrantType {
    #[field(value = "authorization_code")]
    #[serde(rename = "authorization_code")]
    AuthorizationCode,
    #[field(value = "refresh_token")]
    #[serde(rename = "refresh_token")]
    RefreshToken,
}

#[derive(serde::Serialize, serde::Deserialize, rocket::FromForm, Debug)]
pub struct TokenRequest {
    pub client_id: ClientId,
    pub client_secret: ClientSecret,
    pub grant_type: GrantType,
    pub code: AuthorizationCode,
    pub redirect_uri: RedirectUrl,
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct TokenResponse {
    pub access_token: AccessToken,
    pub token_type: String,
    pub expires_in: u32,
    pub refresh_token: RefreshToken,
    pub scope: String,
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct OIDCTokenResponse {
    pub access_token: AccessToken,
    pub token_type: String,
    pub expires_in: u32,
    pub refresh_token: RefreshToken,
    pub id_token: IDToken,
}
