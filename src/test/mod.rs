use super::rocket;
use crate::util;
use rocket::http::Status;
use rocket::local::blocking::Client;

#[test]
fn jwks() {
    let client = Client::tracked(rocket()).expect("valid rocket instance");
    let response = client.get("/.well-known/jwks.json").dispatch();
    assert_eq!(response.status(), Status::Ok);
    assert_eq!(
        response.headers().get("Content-Type").next(),
        Some("application/json")
    );
    assert_eq!(response.into_string().unwrap().starts_with("{"), true);
}

#[test]
fn random() {
    let a = util::random_string(16);
    let b = util::random_string(16);
    assert_ne!(a, b);
    assert_eq!(a.len(), 16);
}
