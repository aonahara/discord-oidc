#[macro_use]
extern crate rocket;

#[cfg(test)]
mod test;

pub mod config;
pub mod oauth2;
pub mod op;

use crate::op::handler;
use dotenv::dotenv;
use std::collections::HashMap;
use std::sync::Arc;
use std::sync::RwLock;

#[launch]
fn rocket() -> _ {
    dotenv().ok();

    rocket::build()
        .manage(reqwest::Client::new())
        .manage(op::AuthState {
            nonce_map: Arc::new(RwLock::new(HashMap::new())),
        })
        .mount(
            "/",
            routes![
                handler::configuration,
                handler::jwks,
                handler::authorization,
                handler::oauth2_callback,
                handler::oauth2_token,
                handler::userinfo,
            ],
        )
}
