use crate::config;
use openidconnect as oidc;
use openidconnect::PrivateSigningKey;

pub fn get_meta() -> oidc::core::CoreProviderMetadata {
    oidc::core::CoreProviderMetadata::new(
        oidc::IssuerUrl::from_url(config::op_issuer_url()),
        oidc::AuthUrl::from_url(config::op_auth_url()),
        oidc::JsonWebKeySetUrl::from_url(config::op_jwk_url()),
        vec![
            // Authorization Code flow
            oidc::ResponseTypes::new(vec![oidc::core::CoreResponseType::Code]),
        ],
        vec![oidc::core::CoreSubjectIdentifierType::Pairwise],
        vec![oidc::core::CoreJwsSigningAlgorithm::RsaSsaPssSha256],
        oidc::EmptyAdditionalProviderMetadata {},
    )
    .set_token_endpoint(Some(oidc::TokenUrl::from_url(config::op_token_url())))
    .set_userinfo_endpoint(Some(oidc::UserInfoUrl::from_url(config::op_userinfo_url())))
    .set_scopes_supported(Some(vec![
        // Only support the "openid" scope for now
        oidc::Scope::new("openid".to_string()),
    ]))
    .set_claims_supported(Some(vec![
        // Providers may also define an enum instead of using CoreClaimName.
        // https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims
        oidc::core::CoreClaimName::new("sub".to_string()),
        oidc::core::CoreClaimName::new("aud".to_string()),
        oidc::core::CoreClaimName::new("exp".to_string()),
        oidc::core::CoreClaimName::new("iat".to_string()),
        oidc::core::CoreClaimName::new("iss".to_string()),
        oidc::core::CoreClaimName::new("name".to_string()),
        oidc::core::CoreClaimName::new("picture".to_string()),
        oidc::core::CoreClaimName::new("locale".to_string()),
    ]))
}

pub fn get_jwks() -> oidc::core::CoreJsonWebKeySet {
    oidc::core::CoreJsonWebKeySet::new(vec![oidc::core::CoreRsaPrivateSigningKey::from_pem(
        &config::jwt_rsa_sk_pem(),
        Some(oidc::JsonWebKeyId::new("primary".to_string())),
    )
    .expect("Invalid RSA private key")
    .as_verification_key()])
}
