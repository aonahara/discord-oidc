use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};

#[derive(Debug)]
pub struct BearerToken<'r> {
    pub token: &'r str,
}

#[derive(Debug)]
pub enum BearerTokenError {
    Missing,
    Invalid,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for BearerToken<'r> {
    type Error = BearerTokenError;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        /// Returns true if `key` is a valid API key string.
        fn is_valid(key: &str) -> bool {
            key.starts_with("Bearer ")
        }

        match req.headers().get_one("authorization") {
            None => Outcome::Failure((Status::BadRequest, BearerTokenError::Missing)),
            Some(key) if is_valid(key) => Outcome::Success(BearerToken { token: &key[7..] }),
            Some(_) => Outcome::Failure((Status::BadRequest, BearerTokenError::Invalid)),
        }
    }
}
