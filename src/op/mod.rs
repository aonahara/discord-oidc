pub mod guard;
pub mod handler;
pub mod meta;
pub mod token;

use crate::oauth2;
use std::collections::HashMap;
use std::sync::Arc;
use std::sync::RwLock;

pub struct AuthState {
    pub nonce_map: Arc<RwLock<HashMap<oauth2::AuthorizationCode, oauth2::Nonce>>>,
}
