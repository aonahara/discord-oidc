use crate::config;
use crate::oauth2;
use crate::oauth2::discord;
use crate::op::guard;
use crate::op::meta;
use crate::op::token;
use crate::op::AuthState;
use openidconnect::core::{CoreJsonWebKeySet, CoreProviderMetadata};
use reqwest::Client;
use rocket::form::Form;
use rocket::http::{Cookie, CookieJar, SameSite, Status};
use rocket::response::Redirect;
use rocket::serde::json::Json;
use url::Url;

macro_rules! must {
    ($expr:expr, $err:expr) => {
        match $expr {
            Err(e) => {
                println!("{:#?}", e);
                return Err($err);
            }
            Ok(val) => val,
        }
    };
    ($expr:expr) => {
        must!($expr, Status::InternalServerError)
    };
}

macro_rules! result_eq {
    ($a:expr, $b:expr) => {
        if $a == $b {
            Ok(())
        } else {
            Err(())
        }
    };
}

#[get("/.well-known/openid-configuration")]
pub fn configuration() -> Json<CoreProviderMetadata> {
    Json(meta::get_meta())
}

#[get("/.well-known/jwks.json")]
pub fn jwks() -> Json<CoreJsonWebKeySet> {
    Json(meta::get_jwks())
}

#[get("/authorize?<scope>&<response_type>&<client_id>&<redirect_uri>&<state>&<nonce>")]
pub fn authorization(
    scope: &str,
    response_type: &str,
    client_id: &str,
    redirect_uri: &str,
    state: Option<&str>,
    nonce: Option<&str>,
    cookies: &CookieJar<'_>,
) -> Result<Redirect, Status> {
    must!(result_eq!(response_type, "code"), Status::NotImplemented);
    must!(
        result_eq!(client_id, config::rp_client_id()),
        Status::Forbidden
    );
    must!(
        result_eq!(redirect_uri, config::rp_redirect_url()),
        Status::Forbidden
    );

    let mut scopes = scope.split(' ');
    if !scopes.any(|x| x == "openid") {
        return Err(Status::BadRequest);
    }

    if let Some(nonce) = nonce {
        println!("[authorize] found nonce: {}", nonce);
        let mut cookie = Cookie::new("oidc_nonce", nonce.to_string());
        cookie.set_same_site(SameSite::None);
        cookie.set_secure(true);
        cookies.add(cookie);
    }

    Ok(Redirect::to(discord::get_oauth2_url(state).to_string()))
}

#[get("/callback?<code>&<state>&<error>&<error_description>")]
pub async fn oauth2_callback(
    code: Option<&str>,
    state: Option<&str>,
    error: Option<&str>,
    error_description: Option<&str>,
    cookies: &CookieJar<'_>,
    auth_state: &rocket::State<AuthState>,
) -> Result<Redirect, Status> {
    // Generate redirect URL
    let mut redir_dest = must!(Url::parse(&config::rp_redirect_url()));
    let mut pairs = redir_dest.query_pairs_mut();
    if let Some(state) = state {
        pairs.append_pair("state", state);
    }
    if let Some(error) = error {
        pairs.append_pair("error", error);
    }
    if let Some(error_description) = error_description {
        pairs.append_pair("error_description", error_description);
    }
    if let Some(code) = code {
        pairs.append_pair("code", code);

        // Store nonce
        if let Some(nonce) = cookies.get("oidc_nonce") {
            println!("[callback] storing nonce: {}", nonce);
            let mut nonce_map = must!(auth_state.nonce_map.write());
            let nonce = nonce.value();
            nonce_map.insert(code.to_string(), nonce.to_string());
        }
    }

    Ok(Redirect::to(pairs.finish().to_string()))
}

#[post("/token", data = "<form>")]
pub async fn oauth2_token(
    form: Form<oauth2::TokenRequest>,
    auth_state: &rocket::State<AuthState>,
    client: &rocket::State<Client>,
) -> Result<Json<oauth2::OIDCTokenResponse>, Status> {
    // Validate client ID and client secret
    println!("[token] validating client id and secret");
    must!(
        result_eq!(form.client_id, config::rp_client_id()),
        Status::Forbidden
    );
    must!(
        result_eq!(form.client_secret, config::rp_client_secret()),
        Status::Forbidden
    );

    println!("[token] exchanging auth code for token");
    let tokens = must!(discord::exchange_code(client, &form.code).await);

    println!("[token] fetching user info");
    let user = must!(discord::get_user_info(client, &tokens.access_token).await);

    // Try to read the nonce
    println!("[token] acquiring lock for nonce");
    let mut nonce_map = must!(auth_state.nonce_map.write());
    let nonce: Option<String> = match nonce_map.remove(&form.code) {
        Some(nonce) => Some(nonce.to_owned()),
        None => None,
    };
    println!("[token] got nonce: {:#?}", nonce);

    println!("[token] making id token");
    let id_token = must!(token::make_id_token(user, nonce));

    let token = oauth2::OIDCTokenResponse {
        access_token: tokens.access_token,
        token_type: tokens.token_type,
        expires_in: tokens.expires_in,
        refresh_token: tokens.refresh_token,
        id_token,
    };

    println!("[token] done");
    Ok(Json(token))
}

#[get("/userinfo")]
pub async fn userinfo(
    access_token: guard::BearerToken<'_>,
    client: &rocket::State<Client>,
) -> Result<Json<token::UserInfo>, Status> {
    println!("[userinfo] fetching discord user info");
    let user = must!(discord::get_user_info(client, access_token.token).await);

    println!("[userinfo] fetching discord guild member");
    let member = must!(discord::get_guild_member(client, &user.id).await);

    println!("[userinfo] generating userinfo json");
    let info = token::make_userinfo(member);
    let infojson = Json(info);

    println!("[userinfo] {:#?}", infojson);
    Ok(infojson)
}
