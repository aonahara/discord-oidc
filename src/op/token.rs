use crate::config;
use crate::oauth2::discord;
use jsonwebtoken::{encode, Algorithm, EncodingKey, Header};
use std::collections::HashMap;

#[derive(serde::Serialize, serde::Deserialize, Clone, Debug)]
pub struct IDToken {
    pub sub: String,
    pub aud: String,
    pub exp: u64,
    pub iat: u64,
    pub iss: String,
    pub name: String,
    pub picture: String,
    pub nonce: Option<String>,
}

#[derive(serde::Serialize, serde::Deserialize, Clone, Debug)]
pub struct UserInfo {
    pub sub: String,
    pub name: String,
    pub picture: String,
    pub guild_id: String,
    pub roles: HashMap<String, bool>,
}

fn make_avatar_url(user: &discord::User) -> String {
    format!(
        "https://cdn.discordapp.com/avatars/{}/{}.png",
        user.id.clone(),
        user.avatar.clone(),
    )
}

pub fn make_id_token(
    user: discord::User,
    nonce: Option<String>,
) -> Result<String, jsonwebtoken::errors::Error> {
    let now = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .expect("whoa, time travel!")
        .as_secs();
    let claims = IDToken {
        sub: user.id.clone(),
        aud: config::rp_client_id(),
        exp: now + 36000,
        iat: now,
        iss: config::op_issuer_url().to_string(),
        name: format!("{}#{}", user.username, user.discriminator),
        picture: make_avatar_url(&user),
        nonce,
    };

    let mut header = Header::new(Algorithm::RS256);
    header.kid = Some("primary".to_owned());

    encode(
        &header,
        &claims,
        &EncodingKey::from_rsa_pem(config::jwt_rsa_sk_pem().as_bytes())?,
    )
}

pub fn make_userinfo(member: discord::GuildMember) -> UserInfo {
    let mut roles = HashMap::new();

    for role in member.roles {
        roles.insert(role, true);
    }

    let userinfo = UserInfo {
        sub: member.user.id.clone(),
        name: format!("{}#{}", member.user.username, member.user.discriminator),
        picture: make_avatar_url(&member.user),
        guild_id: config::discord_guild_id(),
        roles,
    };

    userinfo
}
